import UIKit

class viewControllerEditarPerfil: UIViewController {

    var actualUserId: String?
    var actualUserNombre: String?
    var actualUserClave: String?
    var actualUserEmail: String?
    
    
    @IBOutlet weak var txtNombre: UITextField!
    
    @IBOutlet weak var txtClave: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    
    @IBAction func actualizarTapped(_ sender: Any){
        let nombre = txtNombre.text!
        let clave = txtClave.text!
        let email = txtEmail.text!
        //let datos = ["usuarioId":1, "nombre": "\(nombre)", "genero":"\(genero)", "duracion":"\(duracion)"] as Dictionary<String, Any>
        let datos = ["nombre": "\(nombre)", "clave":"\(clave)", "email":"\(email)"] as Dictionary<String, Any>
        let ruta = "http://localhost:3000/usuarios/\(actualUserId!)"
        metodoPUT(ruta: ruta, datos: datos)
        navigationController?.popViewController(animated: true)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtNombre.text! = actualUserNombre!
        txtClave.text! = actualUserClave!
        txtEmail.text! = actualUserEmail!
        
    }
    
    
    func metodoPUT(ruta: String, datos:[String: Any]){
        let url: URL = URL(string: ruta)!
        var request  = URLRequest(url: url)
        let session = URLSession.shared
        request.httpMethod = "PUT"
        // this is your input parameter dictionary
        let params = datos
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        }catch{
            // catch any exception here
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
                if(data != nil){
                    do{
                        let dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves)
                        print(dict);
                    }catch{
                    // catch any exception here
                    }
                }
            })
        task.resume()
    }
    

}
