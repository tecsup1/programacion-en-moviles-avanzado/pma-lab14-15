
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var txtContrasena: UITextField!
    
    var users = [Users]()
    var actualUserId: String?
    var actualUserNombre: String?
    var actualUserClave: String?
    var actualUserEmail: String?
    
    
    @IBAction func logear(_ sender: Any) {
        let ruta = "http://localhost:3000/usuarios?"
        let usuario = txtUsuario.text!
        let contrasena = txtContrasena.text!
        let url = ruta + "nombre=\(usuario)&clave=\(contrasena)"
        let crearURL = url.replacingOccurrences(of: " ", with: "%20")
        validarUsuario(ruta: crearURL){
            if self.users.count <= 0{
                print("Nombre de usuario y/o contraseña es incorrecto")
            }else{
                print("Logueo exitoso")
                self.performSegue(withIdentifier: "segueLogeo", sender: nil)
                for data in self.users{
                    print("id:\(data.id),nombre:\(data.nombre),nombre:\(data.email)")
                    /*self.actualUserId! = "\(data.id)"
                    self.actualUserNombre! = data.nombre
                    self.actualUserClave! = data.clave
                    self.actualUserEmail! = data.email*/
                }
            }
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueLogeo"{
            let nav = segue.destination  as! UINavigationController
            let siguienteVC = nav.topViewController as! viewControllerBuscar
            //siguienteVC.pelicula = sender as? Peliculas
            
            for data in self.users{
                self.actualUserId = "\(data.id)"
                self.actualUserNombre = data.nombre
                self.actualUserClave = data.clave
                self.actualUserEmail = data.email
            }
            
            
            siguienteVC.actualUserId = actualUserId!
            siguienteVC.actualUserNombre = actualUserNombre!
            siguienteVC.actualUserClave = actualUserClave!
            siguienteVC.actualUserEmail = actualUserEmail!
        }
    
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    func validarUsuario(ruta: String, completed: @escaping ()->()){
        let url = URL(string: ruta)
        URLSession.shared.dataTask(with: url!) {(data, response, error) in
            if error == nil{
                do{
                    self.users = try JSONDecoder().decode([Users].self, from: data!)
                    DispatchQueue.main.async {
                        completed()
                    }
                }catch{
                    print("Error en JSON")
                }
            }
        }.resume()
    }
    
    

}

